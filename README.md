# Tsong

Tsong is a simple program that allows you to play, pause, stop, skip, and adjust the volume of audio files in a specified folder. It uses the Pygame library to handle the audio playback and the os module to interact with the file system.

## Getting Started
- Clone the repository to your local machine.

- Make sure you have Pygame library installed. If not, you can install it by running pip install pygame

- Run the script by running python Tsong.py

- When prompted, enter the location of the folder containing the audio files you want to play. The program will only play mp3 files.

Once the first track has been loaded and started playing, you can use the following commands to control the playback:

play: resume playback
pause: pause playback
stop: stop playback and exit the program
next: skip to the next track
previous: go back to the previous track
current: display the name of the current track
skip: skip to a specific time (in milliseconds)
volume up: increase the volume
volume down: decrease the volume

## License
This project is licensed under the MIT License - see the LICENSE file for details.

### Additional help
Please let me know if you need more help or have any questions about this program.